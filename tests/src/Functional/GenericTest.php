<?php

namespace Drupal\Tests\redirect_after_logout\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for redirect_after_logout.
 *
 * @group cronkeychange
 */
class GenericTest extends GenericModuleTestBase {}
